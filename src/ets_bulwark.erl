-module(ets_bulwark).
-behaviour(gen_server).
%%%
%%%-------------------------------------
%%%  behaviour api
%%%
-export([init/1, handle_call/3, 
	 handle_cast/2, handle_info/2, 
	 terminate/2, code_change/3]).

-export([start_link/1]).

-export([new/1]).
-record(state, {}).

-define(MSG_FMT,"Called ets_bulwark:init/1.~nOwner: ~w.~nTable:~w.~n").


init([State])->
    #{ "owner_atom" := Owner,
       "table" := Table } = State,
    process_flag(trap_exit, true),
    io:format(?MSG_FMT,[Owner,Table]),
    {ok,State}.

new(#{ "name" := TableName,
       "owner_atom" := Owner,
       "bulwark" := Id,
       "ets_spec" := ETS_Spec})->
    Owner_Pid = whereis(Owner),
    gen_server:cast(Id, {new, Owner_Pid,TableName,ETS_Spec});

new(#{ "name" := TableName,
       "owner_pid" := Owner_Pid,
       "bulwark" := Id,
       "ets_spec" := ETS_Spec})->
    
    gen_server:cast(?MODULE, {new, Owner_Pid,TableName,ETS_Spec}).
			

handle_cast({new, Owner_Pid, TableName, ETS_Spec}, State) ->
    Default_Opts = [private,named_table],
    Full_Opts = Default_Opts++ETS_Spec, 
    Table_ID = ets:new(TableName, Full_Opts),
    ets:setopts(Table_ID,[{heir,self(),Full_Opts}]),
    ets:give_away(Table_ID, Owner_Pid, Full_Opts),
    {noreply, State };

handle_cast(_Call, State )->
    {noreply, State }.

%% end handle_cast 

handle_call( _Call, From, State )->
    {reply, From, State }.

%% ETS_GIVE or something goes here
handle_info({'EXIT',Pid,killed}, State)->
    %% TODO: add timestamp; make loggable
    io:format("Table pid: ~w killed.~n",[Pid]),
    {noreply, State};
handle_info({'ETS-TRANSFER', Table_ID, Pid, Data}, State )->
    #{ "owner_atom" := Owner,
       "table" := Table } = State,
    io:format("Received table. Owner: ~w.~n",[Owner]),
    case wait_for_owner(Owner) of
	too_many_calls -> 
	    io:format("Owner of table '~w' registered as '~w' crashed. Exiting.~n",[Table,Owner]),
	    exit(normal);
	OwnerPid -> 
	    ets:give_away(Table_ID, OwnerPid, Data),
	    io:format("Gave table away to ~w.~n",[OwnerPid])
    end,
    {noreply, State}.

wait_for_owner(Owner, 1000)->
    io:format("Exceeded call limit.~n",[]),
    too_many_calls;

wait_for_owner(Owner,Calls) ->
    case whereis(Owner) of
	undefined -> timer:sleep(1),
		     wait_for_owner(Owner,Calls+1);
	Pid -> io:format("Found pid for ~w.~n",[Owner]),
	    Pid
    end.

wait_for_owner(Owner)->
    wait_for_owner(Owner,1).

terminate( _Reason, _State)->
    ok.

code_change(_A,_B,_C)->
    {noreply, _B, _C }.

start_link(State) ->
    #{ "table" := RegisteredAtom } = State,
    gen_server:start_link({local, RegisteredAtom}, 
			  ?MODULE, 
			  [State], 
			  []).
