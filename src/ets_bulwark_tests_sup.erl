-module(ets_bulwark_tests_sup).
-behaviour(supervisor).
%%%-------------------------------------------------------------------
%% @doc <DOCUMENTATION HERE>
%% @end
%%%-------------------------------------------------------------------
-export([start_link/0]).
-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional
-define(CHILD_SPEC,#{id => ets_bulwark_tests,  
		     start => {ets_bulwark_tests,start_link,[]}, 
		     restart => permanent,
		     shutdown => 5000,
		     type => worker,
		     modules => [ets_bulwark_tests]}).


init([]) ->
    io:format("Started supervisor ets_bulwark_tests_sup.~n",[]),
    SupFlags = #{strategy => one_for_one,
                 intensity => 5,
                 period => 10},
    ChildSpecs = [?CHILD_SPEC],
    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
