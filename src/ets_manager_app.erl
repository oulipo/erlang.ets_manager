%%%-------------------------------------------------------------------
%% @doc ets_manager public API
%% @end
%%%-------------------------------------------------------------------

-module(ets_manager_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    ets_manager_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
