%%%-------------------------------------------------------------------
%% @doc ets_manager top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(ets_manager_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional
-define(CHILD_SPEC(ID,TYPE), #{ id=> ID,
			   start => {ID, start_link, []},
			   restart => permanent,
			   shutdown => 5000,
			   type => TYPE,
			   modules => [ID] }).
init([]) ->
    SupFlags = #{strategy => one_for_one,
                 intensity => 0,
                 period => 1},
    ChildSpecs = [ ?CHILD_SPEC(ets_manager,worker)],

    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
