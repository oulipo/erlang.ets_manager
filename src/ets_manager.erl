-module(ets_manager).
-behaviour(gen_server).
%%%
%%%-------------------------------------
%%%  behaviour api
%%%
-export([init/1, handle_call/3, 
	 handle_cast/2, handle_info/2, 
	 terminate/2, code_change/3]).

-export([start_link/0]).

%% handle_cast exports
-export([new/1,new_bulwark/2]).
%% development
-export([test/3]).

-record(state, {table_id}).
-define(CHILD_SPEC(TABLE_NAME,OWNER,STATE), #{ id=> TABLE_NAME,
				   start => {ets_bulwark, start_link, [STATE]},
				   restart => transient,
				   shutdown => brutal_kill,
				   type => worker,
				   modules => [ets_bulwark] }).

init([])->
    io:format("Called ets_manager:init/1.~n",[]),
    process_flag(trap_exit, true),
    {ok,[]}.

%% Exports
new_bulwark(TableName,OwnerAtom)->
    State = #{"table"=> TableName,
	      "owner_atom"=> OwnerAtom},
   supervisor:start_child(ets_manager_sup,?CHILD_SPEC(TableName,OwnerAtom,State)).

new(#{ "name" := TableName,
       "owner_atom" := Owner,
       "bulwark" := BulwarkPid,
       "ets_spec" := ETS_Spec})->
    gen_server:cast(?MODULE, {new, #{ "name" => TableName,
				      "owner_atom" => Owner,
				      "bulwark" => BulwarkPid,
				      "ets_spec" => ETS_Spec}});
new(#{ "name" := TableName,
       "owner_pid" := Owner,
       "bulwark" := BulwarkPid,
       "ets_spec" := ETS_Spec})->
    gen_server:cast(?MODULE, {new,#{ "name" => TableName,
				     "owner_pid" => Owner,
				     "bulwark" => BulwarkPid,
				     "ets_spec" => ETS_Spec}});
new(_Bad_Spec) ->
    {error, unknown_spec}.

%% end Exports
%% Development
test(Name,Owner_Pid,BulwarkPid)->
    new(#{ "name" => Name,
	   "owner_pid" =>Owner_Pid,
	   "bulwark" => BulwarkPid,
	   "ets_spec" => [bag]}).
%% end Development

handle_cast({new, Bulwark_Payload}, State )->
    ets_bulwark:new(Bulwark_Payload),
    send_mesg(Bulwark_Payload),
    {noreply, State };

handle_cast(_Cast, State) ->
    {noreply, State }.

%% end handle_cast exports


send_mesg(TableName,Owner)->
    io:format("Sent ~w to ~w.~n",[TableName,Owner]).
send_mesg(#{"owner_atom":=Owner,"name":=TableName})->
    send_mesg(TableName,Owner);
send_mesg(#{"owner_pid":=Owner,"name":=TableName})->
    send_mesg(TableName,Owner).

handle_call(_Call, From, State) ->
    {reply, From, State }.

handle_info( _Call, State )->
    {noreply, State}.

terminate( _Reason, _State)->
    ok.

code_change(_A,_B,_C)->
    {noreply, _B, _C }.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).
