-module(ets_bulwark_tests).
-behaviour(gen_server).
%%%
%%%-------------------------------------
%%%  behaviour api
%%%
-export([init/1, handle_call/3, 
	 handle_cast/2, handle_info/2, 
	 terminate/2, code_change/3]).

%% ----- External Interface -----
-export([start_link/0]).
%%
-define(SUPERVISOR_CHILD_SPEC, #{ id=> ets_bulwark_tests_sup,
				  start => {ets_bulwark_tests_sup, start_link, []},
				  restart => permanent,
				  shutdown => 5000,
				  type => worker,
				  modules => [ets_bulwark_tests_sup] }).


-export([insert/2,lookup/1,
	 new_table1/0,crash/0,
	 get_pid/0,prepare/0]).

-define(TT1, testtable1).
-define(TT2, testtable2).
-define(TT3, testtable3).
-define(TT4, testtable4).

init([])->
    Pid = self(),
    io:format("ets_bulwark_tests:init/1 has been called.~nPid: ~w.~n",[Pid]),
    process_flag(trap_exit,true),
    {ok,Pid}.

handle_call(new, From, State)->
    {ok, BulwarkPid} = ets_manager:new_bulwark(?TT1,?MODULE),
    ets_manager:new(#{ "name" => ?TT1,
		       "owner_atom" => ?MODULE,
		       "bulwark" => BulwarkPid,
		       "ets_spec" => [bag]}),
    {reply, State, State};

handle_call(crash, From, State)->
    exit(kill),
    {reply, exited, State};

handle_call(get_pid, From, State)->
    {reply, State, State};

handle_call({insert, Row}, From, State)->
    ets:insert(testtable1,Row),
    {reply, From, State};

handle_call({lookup, Key}, From, State)->
    Result = ets:lookup(testtable1,Key),
    {reply, Result, State};
	
handle_call( _Call, From, State )->
    {reply, From, State }.

handle_cast( _Call, State )->
    {noreply, State }.

handle_info( _Call, State )->
    {noreply, State}.

terminate( Reason, State) ->
    io:format("Terminating because ~w.~n",[Reason]),
    ok.

code_change(_A,_B,_C)->
    {noreply, _B, _C }.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

prepare()->
    supervisor:start_child(ets_manager_sup, ?SUPERVISOR_CHILD_SPEC).
new_table1()->
    gen_server:call(?MODULE, new).

crash()->
    gen_server:call(?MODULE, crash).
get_pid()->
    gen_server:call(?MODULE, get_pid).

insert(Key,Value)->
    gen_server:call(?MODULE,{insert, {Key,Value}}).

lookup(Key)->
    gen_server:call(?MODULE,{lookup, Key}).
